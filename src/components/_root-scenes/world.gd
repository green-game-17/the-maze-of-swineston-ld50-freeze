extends Spatial


func _ready():
	pass


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		__on_cancel()


func _process(delta):
	pass


func __on_cancel():
	SCENE.goto_scene('menu')
