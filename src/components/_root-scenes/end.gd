extends Control


func _ready():
	$CenterContainer/ContentContainer/ButtonsContainer/RestartButton.connect('pressed', self, '_on_restart')
	$CenterContainer/ContentContainer/ButtonsContainer/ExitButton.connect('pressed', self, '_on_exit')


func _on_restart():
	SCENE.goto_scene('world')


func _on_exit():
	SCENE.goto_scene('menu')
