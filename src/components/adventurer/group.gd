extends Node

class_name AdventurerGroup


const GROUP_SIZE = 5
const LENGTH_VAR = 0.2
const ROT_VAR = 14

var leader
var members = []
var partner_groups = []
var is_leading_group = true
var offset_positions = []
var next_tile_hist = []


func _ready():
	var reference_vec = Vector2(128, 0).rotated(deg2rad(UTIL.rng.randi() % 360))
	for i in GROUP_SIZE:
		# make length of vector slightly random
		var new_vec = reference_vec + ((reference_vec * UTIL.rng.randf() * LENGTH_VAR) - (LENGTH_VAR / 2))
		# make rotation of vector slightly random
		new_vec = new_vec.rotated(deg2rad(i * (360 / GROUP_SIZE) + ((UTIL.rng.randi() % ROT_VAR) - (ROT_VAR / 2))))
		offset_positions.append(new_vec)
	offset_positions.shuffle()


func set_leader(new_leader):
	leader = new_leader


func get_leader_path() -> AStar2D:
	return leader.astar_node


func is_full(all: bool = false) -> bool:
	var max_capacity = GROUP_SIZE
	var current_capacity = members.size()
	if all:
		for partner_group in partner_groups:
			max_capacity += partner_group.GROUP_SIZE
			current_capacity += partner_group.members.size()
	return max_capacity == current_capacity


# returns the positional offset inside the group
func add_member(new_member, with_pg: bool = false):
	if members.size() == GROUP_SIZE:
		if with_pg:
			for partner_group in partner_groups:
				if not partner_group.is_full():
					new_member.pos_offset = partner_group.add_member(new_member)
					new_member.set_adventurer_group(partner_group)
	else:
		members.append(new_member)
		new_member.pos_offset = offset_positions[members.size() - 1]
		new_member.set_adventurer_group(self)


func add_partner_group(new_partner_group):
	if new_partner_group.is_leading_group and is_leading_group:
		if new_partner_group.partner_groups.size() > partner_groups.size():
			is_leading_group = false
			new_partner_group.add_partner_group(self)
			for partner_group in partner_groups:
				new_partner_group.add_partner_group(partner_group)
				for in_partner_group in new_partner_group.partner_groups:
					in_partner_group.add_partner_group(partner_group)
		else:
			new_partner_group.is_leading_group = false
			add_partner_group(new_partner_group)
			for partner_group in new_partner_group.partner_groups:
				add_partner_group(partner_group)
				for in_partner_group in partner_groups:
					in_partner_group.add_partner_group(partner_group)
	else:
		partner_groups.append(new_partner_group)


func share_paths(astar_node: AStar2D):
	leader.refresh_path(astar_node)
	for member in members:
		member.refresh_path(astar_node)


func share_next_tile(tile: Vector2, with_leader: bool = false):
	next_tile_hist.push_front(tile)
	if with_leader:
		leader.set_next_tile(tile)
	for member in members:
		member.set_next_tile(tile)
	if is_leading_group:
		for index in partner_groups.size():
			var next_tile
			if next_tile_hist.size() <= index:
				next_tile = next_tile_hist[-1]
			else:
				next_tile = next_tile_hist[index]
			partner_groups[index].share_next_tile(next_tile, true)


func remove_latest_tile():
	next_tile_hist.pop_front()


func split_up():
	var leading_group = get_leading_group()
	var group_index = leading_group.partner_groups.find(self)
	if group_index != -1:
		var new_partner_groups = []
		for i in range(group_index + 1, leading_group.partner_groups.size()):
			new_partner_groups.append(leading_group.partner_groups[i])
		for partner_group in new_partner_groups:
			leading_group.remove_partner_group(partner_group)
		leading_group.sync_partner_groups()
		partner_groups = new_partner_groups
		sync_partner_groups()
		is_leading_group = true


func remove_partner_group(partner_group):
	partner_groups.erase(partner_group)


func sync_partner_groups():
	for partner_group in partner_groups:
		partner_group.partner_groups = [self]
		for new_partner_group in partner_groups:
			if partner_group != new_partner_group:
				partner_group.add_partner_group(new_partner_group)


func get_leading_group() -> AdventurerGroup:
	for partner_group in partner_groups:
		if partner_group.is_leading_group:
			return partner_group
	return self


func share_trap(trap_tile: Vector2, pos_tile: Vector2, with_leader: bool = false):
	if with_leader:
		leader.set_tile_as_trap(trap_tile, pos_tile)
	for member in members:
		member.set_tile_as_trap(trap_tile, pos_tile)
	if is_leading_group:
		for partner_group in partner_groups:
			partner_group.share_trap(trap_tile, pos_tile, true)


func get_class_ids() -> Array:
	var class_ids = [ID.TEAMLEADER]
	for member in members:
		class_ids.append(member.class_id)
	return class_ids


func get_all_class_ids() -> Array:
	var class_ids = get_class_ids()
	for partner_group in partner_groups:
		class_ids.append_array(partner_group.get_class_ids())
	return class_ids


func get_group_strength(with_pg: bool = false):
	var strength = leader.strength
	for member in members:
		strength += member.strength
	if with_pg:
		for partner_group in partner_groups:
			strength += partner_group.get_group_strength()
	return strength


func group_death(with_pg: bool = false, clean_up: bool = false):
	if clean_up:
		for partner_group in partner_groups:
			partner_group.partner_groups.erase(self)
	if with_pg:
		EVENTS.emit_signal('adventurer_died', get_all_class_ids())
		for partner_group in partner_groups:
			partner_group.group_death()
	for member in members:
		member.queue_free()
	leader.queue_free()
	queue_free()


func can_disarm_trap(trap_id: int) -> bool:
	return DATA.can_disarm_trap(get_all_class_ids(), trap_id)


func spots_trap(trap_id: int) -> bool:
	return DATA.spots_trap(get_all_class_ids(), trap_id)
