extends Control


onready var construct_button = $ButtonPanel/ButtonsContainer/ConstructButton
onready var destruct_button = $ButtonPanel/ButtonsContainer/DestructButton
onready var construct_banner = $ConstructBanner
onready var destruct_banner = $DestructBanner
onready var panel = $Panel
onready var traps_button = $Panel/Container/ButtonsContainer/TrapsButton
onready var monsters_button = $Panel/Container/ButtonsContainer/MonstersButton
onready var extend_button = $ButtonPanel/ButtonsContainer/ExtendButton
onready var cards_container = $Panel/Container/CardsContainer

onready var card_scene = load('res://components/entity_picker/entity_card.tscn')


func _ready():
	construct_button.connect('pressed', self, '_on_construct_pressed')
	destruct_button.connect('pressed', self, '_on_destruct_pressed')

	traps_button.connect('pressed', self, '_on_traps_pressed')
	monsters_button.connect('pressed', self, '_on_monsters_pressed')
	extend_button.connect('pressed', self, '_on_extend_pressed')

	EVENTS.connect('abort_entity_placing', self, '_on_finish_construct', [null, null])
	STATE.connect('maze_trap_placed', self, '_on_finish_construct', [null])
	STATE.connect('maze_monster_placed', self, '_on_finish_construct')

	EVENTS.connect('abort_tile_deleting', self, '_on_finish_destruct', [null])
	STATE.connect('maze_trap_placed', self, '_on_finish_destruct')

	STATE.connect('souls_count_changed', self, '_on_souls_count_changed')

	_on_traps_pressed() # init data


func _input(_event):
	if Input.is_action_just_pressed('ui_construct'):
		_on_construct_pressed()
	elif Input.is_action_just_pressed('ui_destruct'):
		_on_destruct_pressed()


func _on_construct_pressed():
	if destruct_banner.visible:
		destruct_banner.hide()
		destruct_button.pressed = false
		EVENTS.emit_signal('abort_tile_deleting')

	EVENTS.emit_signal('abort_entity_placing')

	__toggle_construct_ui()


func _on_destruct_pressed():
	if panel.visible:
		__toggle_construct_ui()
		EVENTS.emit_signal('abort_entity_placing')

	if destruct_banner.visible:
		destruct_banner.hide()
		EVENTS.emit_signal('abort_tile_deleting')
	else:
		destruct_banner.show()
		EVENTS.emit_signal('start_tile_deleting')


func _on_traps_pressed():
	traps_button.pressed = true
	monsters_button.pressed = false

	UTIL.delete_all_children(cards_container)

	for id in DATA.TRAPS:
		var new_card = card_scene.instance()
		var data = DATA.TRAPS[id]
		new_card.set_trap_data(data)
		cards_container.add_child(new_card)
		new_card.connect('pressed', self, '_on_card_pressed', [id, data])


func _on_monsters_pressed():
	monsters_button.pressed = true
	traps_button.pressed = false

	UTIL.delete_all_children(cards_container)

	for id in DATA.MONSTERS:
		var new_card = card_scene.instance()
		var data = DATA.MONSTERS[id]
		new_card.set_monster_data(data)
		cards_container.add_child(new_card)
		new_card.connect('pressed', self, '_on_card_pressed', [id, data])


func _on_card_pressed(id, data):
	__toggle_construct_ui()
	construct_banner.show()

	EVENTS.emit_signal('start_entity_placing', id, data)


func _on_finish_construct(_a1, _a2):
	construct_banner.hide()


func _on_finish_destruct(_a1):
	destruct_banner.hide()
	destruct_button.pressed = false


func _on_souls_count_changed(count):
	if count > (STATE.get_goal_tile().y+1) / STATE.MAZE_HEIGHT_PER_CHUNK * DATA.SOULS_PER_EXTEND:
		extend_button.disabled = false
		extend_button.mouse_default_cursor_shape = CURSOR_POINTING_HAND
	else:
		extend_button.disabled = true
		extend_button.mouse_default_cursor_shape = CURSOR_FORBIDDEN


func _on_extend_pressed():
	var souls = STATE.get_souls_count()
	var target_height = (souls/DATA.SOULS_PER_EXTEND+1)*STATE.MAZE_HEIGHT_PER_CHUNK
	while STATE.get_goal_tile().y+1 < target_height:
		STATE.extend_maze()

	extend_button.disabled = true
	extend_button.mouse_default_cursor_shape = CURSOR_FORBIDDEN


func __toggle_construct_ui():
	if panel.visible:
		construct_banner.hide()
		panel.hide()
		construct_button.pressed = false
	else:
		construct_banner.show()
		panel.show()
		construct_button.pressed = true
