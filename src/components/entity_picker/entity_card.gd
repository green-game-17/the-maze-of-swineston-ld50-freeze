extends Control

signal pressed()


var disabled = false
var cost = 0


func set_trap_data(data: Dictionary):
	__set_data(data)

	$Container/IconContainer/DurabilityContainer/Label.set_text('%d' % data.durability)

	var spot_texture1 = null
	var spot_texture2 = null
	if data.spotted_by.size() >= 1:
		spot_texture1 = DATA.CLASSES[data.spotted_by[0]].sprites.down
	if data.spotted_by.size() >= 2:
		spot_texture2 = DATA.CLASSES[data.spotted_by[1]].sprites.down

	var disarm_texture1 = null
	var disarm_texture2 = null
	if data.disarmed_by.size() >= 1:
		disarm_texture1 = DATA.CLASSES[data.disarmed_by[0]].sprites.down
	if data.disarmed_by.size() >= 2:
		disarm_texture2 = DATA.CLASSES[data.disarmed_by[1]].sprites.down

	if spot_texture1:
		$Container/TextContainer/ClassesIconsContainer/SpotClassesContainer/Icon1.set_texture(spot_texture1)
	else:
		$Container/TextContainer/ClassesIconsContainer/SpotClassesContainer/Icon1.hide()
	if spot_texture2:
		$Container/TextContainer/ClassesIconsContainer/SpotClassesContainer/Icon2.set_texture(spot_texture2)
	else:
		$Container/TextContainer/ClassesIconsContainer/SpotClassesContainer/Icon2.hide()

	if disarm_texture1:
		$Container/TextContainer/ClassesIconsContainer/DisarmClassesContainer/Icon1.set_texture(disarm_texture1)
	else:
		$Container/TextContainer/ClassesIconsContainer/DisarmClassesContainer/Icon1.hide()
	if disarm_texture2:
		$Container/TextContainer/ClassesIconsContainer/DisarmClassesContainer/Icon2.set_texture(disarm_texture2)
	else:
		$Container/TextContainer/ClassesIconsContainer/DisarmClassesContainer/Icon2.hide()


func set_monster_data(data: Dictionary):
	__set_data(data)

	$Container/IconContainer/DurabilityContainer/Label.set_text('%d' % data.strength)

	$Container/TextContainer/ClassesTitlesContainer.hide()
	$Container/TextContainer/ClassesIconsContainer.hide()


func __set_data(data: Dictionary):
	$Container/IconContainer/Icon.set_texture(data.icon)
	$Container/TextContainer/Title.set_text(data.name)
	$Container/TextContainer/Description.set_text(data.description)
	$Container/IconContainer/CostContainer/Label.set_text('%d' % data.cost)

	cost = data.cost

	__set_disabled(STATE.get_life_force_count())

	connect('gui_input', self, '_on_input')
	STATE.connect('life_force_count_changed', self, '__set_disabled')


func _on_input(event):
	if not disabled and event is InputEventMouseButton and event.get_button_index() == BUTTON_LEFT and event.pressed:
		emit_signal('pressed')


func __set_disabled(life_force_count):
	if life_force_count >= cost:
		disabled = false
		modulate.a = 1.0
		mouse_default_cursor_shape = CURSOR_POINTING_HAND
	else:
		disabled = true
		modulate.a = 0.5
		mouse_default_cursor_shape = CURSOR_FORBIDDEN
