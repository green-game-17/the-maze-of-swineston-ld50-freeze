extends Node


onready var rng = RandomNumberGenerator.new()


func _ready():
	rng.randomize()
	randomize()


func filled_matrix(width: int, height: int, default_value):
	var matrix = []
	for iy in range(height):
		matrix.append([])
		for _ix in range(width):
			if typeof(default_value) == TYPE_DICTIONARY:
				matrix[iy].append(default_value.duplicate(true))
			else:
				matrix[iy].append(default_value)
	return matrix


func delete_all_children(node):
	for child in node.get_children():
		node.remove_child(child)
		child.queue_free()
