extends Node

# Constant Identifiers:

# Tile types
const ENTRANCE = 0
const GOAL = 1

# Class types
const TEAMLEADER = 100
const ROGUE = 101
const MAGICIAN = 102
const WARRIOR = 103
const GUNNER = 104

# Traps
const BANANA_PEEL = 201
const FIRE_PIT = 202
const BEAR_TRAP = 203
const FIGHT_ME_PRINTER = 204
const BOX = 205
const PLANT = 206
const SPIKES = 207

# Monsters
const SNAKE_COAT = 305
const LIZZARDMAN = 306
const ROBO_PENGUIN = 307
const SUSHI_CHEF = 308
const DISTORTED_SOUL = 309
