extends Node

# Static, shared data:
const SPOT_CHANCE_BASIC = 0.1
const SPOT_CHANCE_SPECIALIZED = 0.25

const TILE_SIZE = 3
const CELL_PX_SIZE = 128

const GRID_SIZE = TILE_SIZE + 1
const TILE_PX_SIZE = CELL_PX_SIZE * TILE_SIZE
const GRID_PX_SIZE = CELL_PX_SIZE * GRID_SIZE

const TARGET_SOULS_COUNT = 1000
const SOULS_PER_EXTEND = 100

const LIFE_FORCE_PER_WALL_MOVE = 100

const DESTRUCT_LIFE_FORCE_RESTORE_SHARE = 0.2


var CLASSES = {
	ID.TEAMLEADER: {
		name = 'Teamleader',
		strength = 2,
		life_force = 30,
		sprites = {
			up = load('res://assets/chars/leader/leader_back.png'),
			right = load('res://assets/chars/leader/leader_right.png'),
			down = load('res://assets/chars/leader/leader_front.png'),
			left = load('res://assets/chars/leader/leader_left.png')
		}
	},
	ID.ROGUE: {
		name = 'Rogue',
		strength = 3,
		life_force = 20,
		sprites = {
			up = load('res://assets/chars/rogue/rogue_back.png'),
			right = load('res://assets/chars/rogue/rogue_right.png'),
			down = load('res://assets/chars/rogue/rogue_front.png'),
			left = load('res://assets/chars/rogue/rogue_left.png')
		}
	},
	ID.MAGICIAN: {
		name = 'Magician',
		strength = 5,
		life_force = 60,
		sprites = {
			up = load('res://assets/chars/wizard/wizard_back.png'),
			right = load('res://assets/chars/wizard/wizard_right.png'),
			down = load('res://assets/chars/wizard/wizard_front.png'),
			left = load('res://assets/chars/wizard/wizard_left.png')
		}
	},
	ID.WARRIOR: {
		name = 'Warrior',
		strength = 8,
		life_force = 100,
		sprites = {
			up = load('res://assets/chars/warrior/warrior_back.png'),
			right = load('res://assets/chars/warrior/warrior_right.png'),
			down = load('res://assets/chars/warrior/warrior_front.png'),
			left = load('res://assets/chars/warrior/warrior_left.png')
		}
	},
	ID.GUNNER: {
		name = 'Gunner',
		strength = 4,
		life_force = 50,
		sprites = {
			up = load('res://assets/chars/agent/agent_back.png'),
			right = load('res://assets/chars/agent/agent_right.png'),
			down = load('res://assets/chars/agent/agent_front.png'),
			left = load('res://assets/chars/agent/agent_left.png')
		}
	}
}

var TRAPS = {
	ID.BANANA_PEEL: {
		name = 'Banana Peel',
		description = 'Caution, very very slippery',
		icon = load('res://assets/traps/banana_peel/banana_peel_icon.png'),
		sprites = {
			normal = load('res://assets/traps/banana_peel/banana_peel_normal.png'),
			active = load('res://assets/traps/banana_peel/banana_peel_active.png'),
			destroyed = load('res://assets/traps/banana_peel/banana_peel_destroy.png')
		},
		cost = 5,
		durability = 1,
		spotted_by = [ID.ROGUE],
		disarmed_by = [ID.ROGUE]
	},
	ID.FIRE_PIT: {
		name = 'Fire Pit',
		description = 'Ahhh, that\'s hot!',
		icon = load('res://assets/traps/fire_pit/fire_pit_icon.png'),
		sprites = {
			normal = load('res://assets/traps/fire_pit/fire_pit_normal.png'),
			active = load('res://assets/traps/fire_pit/fire_pit_active.png'),
			destroyed = load('res://assets/traps/fire_pit/fire_pit_destroy.png')
		},
		cost = 90,
		durability = 10,
		spotted_by = [ID.GUNNER, ID.WARRIOR],
		disarmed_by = [ID.GUNNER]
	},
	ID.BEAR_TRAP: {
		name = 'Bear Trap',
		description = 'Pretty pointy teeth',
		icon = load('res://assets/traps/bear_trap/bear_trap_icon.png'),
		sprites = {
			normal = load('res://assets/traps/bear_trap/bear_trap_normal.png'),
			active = load('res://assets/traps/bear_trap/bear_trap_active.png'),
			destroyed = load('res://assets/traps/bear_trap/bear_trap_destroy.png')
		},
		cost = 65,
		durability = 1,
		spotted_by = [ID.WARRIOR],
		disarmed_by = [ID.WARRIOR, ID.WARRIOR]
	},
	ID.BOX: {
		name = 'Mimic Box',
		description = 'Loves to devour adventurers which it lures by its fake appearance',
		icon = load('res://assets/traps/box/box_icon.png'),
		sprites = {
			normal = load('res://assets/traps/box/box_normal.png'),
			active = load('res://assets/traps/box/box_active.png'),
			destroyed = load('res://assets/traps/box/box_destroy.png')
		},
		cost = 65,
		durability = 1,
		spotted_by = [ID.MAGICIAN],
		disarmed_by = [ID.MAGICIAN]
	},
	ID.PLANT: {
		name = 'Carniverous Plant',
		description = 'Not your average house plant',
		icon = load('res://assets/ui/plant_icon.png'),
		sprites = {
			normal = load('res://assets/traps/plant/plant_normal.png'),
			active = load('res://assets/traps/plant/plant_active.png'),
			destroyed = load('res://assets/traps/plant/plant_destroy.png')
		},
		cost = 90,
		durability = 1,
		spotted_by = [ID.ROGUE],
		disarmed_by = [ID.ROGUE, ID.ROGUE]
	},
		ID.SPIKES: {
		name = 'Floor Spikes',
		description = 'Cannot see a point to them honestly',
		icon = load('res://assets/ui/spikes_icon.png'),
		sprites = {
			normal = load('res://assets/traps/spikes/spikes_normal.png'),
			active = load('res://assets/traps/spikes/spikes_active.png'),
			destroyed = load('res://assets/traps/spikes/spikes_destroy.png')
		},
		cost = 5,
		durability = 1,
		spotted_by = [ID.WARRIOR],
		disarmed_by = [ID.WARRIOR]
	}
	# ID.FIGHT_ME_PRINTER: {
	# 	name = 'Fight Me Printer',
	# 	description = 'Fueled by the anger of needing cyan',
	# 	icon = load(''),
	# 	image = load(''),
	#   durability = 1,
	#   specialization = [],
	#   can_disarm = []
	# }
}

var MONSTERS = {
	ID.SNAKE_COAT: {
		name = 'Five Snakes in a trenchcoat',
		description = 'No more text needed',
		icon = load('res://assets/ui/five_snake_icon.png'),
		sprites = {
			up = load('res://assets/monsters/snake/five_snake_back.png'),
			right = load('res://assets/monsters/snake/five_snake_right.png'),
			down = load('res://assets/monsters/snake/five_snake_front.png'),
			left = load('res://assets/monsters/snake/five_snake_left.png')
		},
		cost = 10,
		strength = 5
	},
	ID.LIZZARDMAN: {
		name = 'Lizzardman',
		description = 'I am Pig, yes. Lets go skateboard',
		icon = load('res://assets/ui/lizardman_icon.png'),
		sprites = {
			up = load('res://assets/monsters/lizardman/snake_back.png'),
			right = load('res://assets/monsters/lizardman/snake_right.png'),
			down = load('res://assets/monsters/lizardman/snake_front.png'),
			left = load('res://assets/monsters/lizardman/snake_left.png')
		},
		cost = 10,
		strength = 1
	},
	ID.ROBO_PENGUIN: {
		name = 'Robot Penguin',
		description = 'Beep boop, turn the adventurer into moot',
		icon = load('res://assets/ui/robot_penguin_icon.png'),
		sprites = {
			up = load('res://assets/monsters/robot_penguin/robot_penguin_back.png'),
			right = load('res://assets/monsters/robot_penguin/robot_penguin_right.png'),
			down = load('res://assets/monsters/robot_penguin/robot_penguin_front.png'),
			left = load('res://assets/monsters/robot_penguin/robot_penguin_left.png')
		},
		cost = 80,
		strength = 10
	},
	ID.SUSHI_CHEF: {
		name = 'Sushi Chef',
		description = 'After many years in the kitchen, their knife is still as sharp as ever',
		icon = load('res://assets/ui/chef_icon.png'),
		sprites = {
			up = load('res://assets/monsters/chef/chef_back.png'),
			right = load('res://assets/monsters/chef/chef_right.png'),
			down = load('res://assets/monsters/chef/chef_front.png'),
			left = load('res://assets/monsters/chef/chef_left.png')
		},
		cost = 80,
		strength = 10
	},
	ID.DISTORTED_SOUL: {
		name = 'Distorted Soul',
		description = 'a.k.a. a ghost which lost its soul fragments',
		icon = load('res://assets/ui/phantom_icon.png'),
		sprites = {
			up = load('res://assets/monsters/phantom/phantom_back.png'),
			right = load('res://assets/monsters/phantom/phantom_right.png'),
			down = load('res://assets/monsters/phantom/phantom_front.png'),
			left = load('res://assets/monsters/phantom/phantom_left.png')
		},
		cost = 1,
		strength = 5
	},
}


func can_disarm_trap(class_ids: Array, trap_id: int) -> bool:
	var disarmed_by = TRAPS[trap_id].disarmed_by.duplicate()
	for class_id in class_ids:
		if class_id in disarmed_by:
			disarmed_by.erase(class_id)
			if disarmed_by.size() == 0:
				return true
	return false


func spots_trap(class_ids: Array, trap_id: int) -> bool:
	var spotted_by = TRAPS[trap_id].spotted_by.duplicate()
	var chance_specialized = 0
	var chance_basic = 0
	for class_id in class_ids:
		if class_id in spotted_by:
			chance_specialized += SPOT_CHANCE_SPECIALIZED
		else:
			chance_basic += SPOT_CHANCE_BASIC
	if chance_specialized > 0:
		if UTIL.rng.randf() <= chance_specialized:
			return true
	elif chance_basic > 0:
		if UTIL.rng.randf() <= chance_basic:
			return true
	return false


func get_strength(type_id: int) -> int:
	if type_id in MONSTERS:
		return MONSTERS[type_id].strength
	elif type_id in CLASSES:
		return CLASSES[type_id].strength
	return 0


func get_sprites(type_id: int) -> Dictionary:
	if type_id in CLASSES:
		return CLASSES[type_id].sprites
	elif type_id in MONSTERS:
		return MONSTERS[type_id].sprites
	elif type_id in TRAPS:
		return TRAPS[type_id].sprites
	return {}


func get_durability(trap_id: int) -> int:
	return TRAPS[trap_id].durability


func get_life_force(type_id: int) -> int:
	if type_id in CLASSES:
		return CLASSES[type_id].life_force
	elif type_id in TRAPS:
		return TRAPS[type_id].cost
	elif type_id in MONSTERS:
		return MONSTERS[type_id].cost
	return 0


func get_monster(id: int) -> Dictionary:
	return MONSTERS[id]


func get_trap(id: int) -> Dictionary:
	return TRAPS[id]


func get_classes() -> Array:
	return CLASSES.keys()
