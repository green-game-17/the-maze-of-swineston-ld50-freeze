# Dev Tips

### Importing magica models
1. export from magica voxel as obj
1. open `raw_assets/blender_helper.blend` in blender
1. import the obj
1. select imported model and export as gltf into `src/assets`, leave settings as-is
1. open the glb in godot, selecting "New Inherited"
1. select the "ObjObject" node
1. in the properties on the right, click on the "Mesh" preview, than open the "Surface 1" section and click on the "Material" preview
1. ensure that nothing in the "Vertex Colors" section is checked
1. open the "Albedo" section and click on the "Texture" preview
1. ensure that "Filter" *is not* checked and that "sRGB" *is* checked
